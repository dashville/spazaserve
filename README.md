#Spazaserve shopping system:

created by Dashville Rikhotso

Spazaserve is basic shopping system and consist of two parts, server-side and client-side:
	-Server-side is NodeJs is server
	-The client is Angular

#prerequisites
1. Angular 6 is required.
2. Any node version greater than v9 is required, version 10.0.0 used in development;
3. MongoDB is required, version 3.2.17 used in development.

#TO get started;
1. open terminal and run mongod, if mongo service is not already running.
2. open another terminal and navigate to spazaserve directory
3. run npm install
4. run npm start
5. open another terminal navigate to spazaserve/client directory
6. run npm install
7. run npm start
8. open another terminal and navigate to spazaserve/scripts directory
9. run the following startup script once:

	mongo 127.0.0.1/spaza_db startup.sh

	if a mistake was made when using startup.sh, run drop.sh, then run startup after.

#Now you can use the system.

1.signup to use
2. Use the following credentials to sign in as admin:

   username: admin@admin.com
   password: 1234

