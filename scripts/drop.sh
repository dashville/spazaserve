#!/bin/sh
db.users.drop()
db.accounts.drop()
db.discounts.drop()
db.products.drop()
db.purchases.drop()