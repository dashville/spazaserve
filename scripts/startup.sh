#!/bin/sh
db.users.insert({
	 "admin" : true,
	 "roles" : [ { "role" : "user", "description" : "User" } ],
	 "email" : "admin@admin.com", "username" : "admin@admin.com",
	 "password" : "$2a$10$WumtXikjdVTaja8RY9gyK.bdp3/mCKFuAdNe.7lV5AabT7cRadL.e",
	 "name" : "spaza", "lastname" : "admin",
	 "phone" : "+27721011010",
	 "createdOn": "1526861301"
})

db.discounts.insert({ "createdOn": "1526861301",rule:'between 50 and 100, both inclusive', rate: 0})
db.discounts.insert({ "createdOn": "1526861301",rule: 'between 112 and 115, both inclusive', rate: 25})
db.discounts.insert({ "createdOn": "1526861301",rule:'greater than 120', rate: 50})

db.products.insert({"createdOn" : 1526861301, "brand" : "Coca-cola", "name" : "Sprite", "description" : "Sugar drink", "price" : 45, })
db.products.insert({"createdOn" : 1526861301, "brand" : "Coca-cola", "name" : "coke zero", "price" : 112})
db.products.insert({"createdOn" : 1526861301, "brand" : "Magnum", "name" : "Mint", "description" : "ice cream", "price" : 123})
db.products.insert({"createdOn" : 1526861301, "brand" : "Magnum", "name" : "Death by chocolate", "description" : "ice cream", "price" : 25.99})