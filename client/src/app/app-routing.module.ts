import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {SignupComponent} from './components/signup/signup.component';
import {ProductsComponent} from './components/products/products.component';
import {AccountComponent} from './components/account/account.component';
import {PurchaseComponent} from './components/products/components/purchase/purchase.component'
import {PurchasesComponent} from './components/admin/purchases/purchases.component';
import {DiscountComponent} from './components/admin/discount/discount.component';
import {AdminProductsComponent} from './components/admin/products/admin.products.component';
import {AddproductComponent} from './components/admin/products/addproduct/addproduct.component';
import {UpdateproductComponent} from './components/admin/products/updateproduct/updateproduct.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: 'signup', component: SignupComponent},
  { path: 'products', component: ProductsComponent},
  { path: 'account', component: AccountComponent},
  { path: 'product/purchase', component: PurchaseComponent},
  { path: 'admin/purchases', component: PurchasesComponent},
  { path: 'admin/discounts', component: DiscountComponent},
  { path: 'admin/products', component: AdminProductsComponent},
  { path: 'admin/product/add', component: AddproductComponent},
  { path: 'admin/product/update', component: UpdateproductComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
