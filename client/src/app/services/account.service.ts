import {Injectable} from '@angular/core';
import _ from 'lodash';

import {ApiService} from './api.service';
import {AppService} from './app.service';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  account;

  constructor(private apiService: ApiService, private appService: AppService) {
  }

  accountTopup(id, amount) {
    const app = this.appService.getApp();
    return new Promise((resolve, reject) => {
      this.apiService.accountTopup(app, {id, amount}).subscribe(responseData => {
        if (responseData['success']) {
           resolve(responseData['item']);
        } else {
           reject(responseData['message']);
        }
      });
    });
  }

  getAccount() {
    const app = this.appService.getApp();
    return new Promise((resolve, reject) => {
      this.apiService.getAccount(app).subscribe(responseData => {
        if (responseData['success']) {
           resolve(responseData['item']);
        } else {
           reject(responseData['message']);
        }
      });
    });
  }

  setAccount(account) {
    this.account = _.extend({}, this.account, account);
  }
}
