import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {ApiService} from './api.service';
import {AppService} from './app.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private apiService: ApiService, private appService: AppService, private router: Router) {
  }

  login(credentials) {
    const app = this.appService.getApp();
     return new Promise((resolve, reject) => {

       this.apiService.login(app, credentials).subscribe((responseData) => {
         if (responseData['success']) {
           this.loginSuccessful(responseData);
           resolve(true);
         } else {
           reject(responseData['message']);
         }
       });

     });
  }
  logout(){
    return this.appService.resetApp();
  }

  loginSuccessful(data) {
    this.appService.onUserChanged(data.item);
    this.appService.tokenSave(data.token);

    this.router.navigate(['/products'], {replaceUrl: true});
  }

  singup(data) {
    const app = this.appService.getApp();
    return new Promise((resolve, reject) => {

      this.apiService.signup(app, data).subscribe((responseData) => {
        if (responseData['success']) {
          this.loginSuccessful(responseData);
          resolve(true);
        } else {
          reject(responseData['message']);
        }
      });

    });
  }
}
