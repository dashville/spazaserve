import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import _ from 'lodash';

import {ApiService} from './api.service';
import {AppService} from './app.service';

@Injectable({
  providedIn: 'root'
})

export class ProductService {
  products;
  user;

  constructor(private apiService: ApiService, private appService: AppService, private router: Router) {
    this.appService.castUser.subscribe((user) => {
      this.user = user;
    });
  }

  getProducts() {
    const app = this.appService.getApp();
    return this.apiService.getProducts(app);
  }

  getProduct(id) {
    const that = this;
    return new Promise((resolve, reject) => {
      const find = () => {
        return _.find(that.products, product => product.id === id);
      };
      if (this.products) {
        resolve(find());
      } else {
        this.getProducts().subscribe((responseData) => {
          if (responseData['success']) {
            that.products = responseData['items'];
            resolve(find());
          } else {
            alert('No product');
            that.router.navigate(['/admin/purchases']);
            reject('failed');
          }
        });
      }
    });
  }

  purchaseProduct(product, quantity) {
    const app = this.appService.getApp();
    return this.apiService.purchaseProduct(app, {user: this.user, product, quantity});
  }

  setProducts(products = []) {
    this.products = products;
  }
}
