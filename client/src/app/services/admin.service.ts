import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {AppService} from './app.service';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private apiService: ApiService, private appService: AppService) {
  }

  addProduct(data) {
    const app = this.appService.getApp();
    return this.apiService.addProduct(app, data);
  }

  getDiscounts() {
    const app = this.appService.getApp();
    return this.apiService.getDiscounts(app);
  }

  getPurchases() {
    const app = this.appService.getApp();
    return this.apiService.getPurchases(app);
  }

  removeProduct(id) {
    const app = this.appService.getApp();
    return this.apiService.removeProduct(app, id);
  }

  updateDiscount(id, rate) {
    const app = this.appService.getApp();
    return this.apiService.updateDiscount(app, {id, rate});
  }

  updateProduct(id, updates) {
    const app = this.appService.getApp();
    return this.apiService.updateProduct(app, id, updates);
  }
}
