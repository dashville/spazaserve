import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  apiUrl: string;
  constructor(private http: HttpClient) {

  }
  // admin user
  // ==================================================
  addProduct(app, data) {
    const url = `${app.apiUrl}/admin/product/add`;
    const headers = app.requestHeaders();
    const httpOptions = {
      headers: new HttpHeaders(
        _.extend({}, headers, {
        'Accept': 'application/json', 'Content-Type': 'application/json'
      }))
    };
    return this.http.post(url, data, httpOptions);
  }
  getDiscounts(app) {
    const url = `${app.apiUrl}/admin/discounts`;
    const headers = app.requestHeaders();
    const httpOptions = {
      headers: new HttpHeaders(
        _.extend({}, headers, {
        'Accept': 'application/json', 'Content-Type': 'application/json'
      }))
    };
    return this.http.get(url, httpOptions);
  }
  getPurchases(app){
    const url = `${app.apiUrl}/admin/purchases`;
    const headers = app.requestHeaders();
    const httpOptions = {
      headers: new HttpHeaders(
        _.extend({}, headers, {
        'Accept': 'application/json', 'Content-Type': 'application/json'
      }))
    };
    return this.http.get(url, httpOptions);
  }
  removeProduct(app, id) {
    const url = `${app.apiUrl}/admin/product/${id}/remove`;
    const headers = app.requestHeaders();
    const httpOptions = {
      headers: new HttpHeaders(
        _.extend({}, headers, {
        'Accept': 'application/json', 'Content-Type': 'application/json'
      }))
    };
    return this.http.delete(url, httpOptions);
  }
  updateDiscount(app, data) {
    const url = `${app.apiUrl}/admin/discount/update`;
    const headers = app.requestHeaders();
    const httpOptions = {
      headers: new HttpHeaders(
        _.extend({}, headers, {
        'Accept': 'application/json', 'Content-Type': 'application/json'
      }))
    };
    return this.http.patch(url, data, httpOptions);
  }
  updateProduct(app, id, updates) {
    const url = `${app.apiUrl}/admin/product/${id}/update`;
    const headers = app.requestHeaders();
    const httpOptions = {
      headers: new HttpHeaders(
        _.extend({}, headers, {
        'Accept': 'application/json', 'Content-Type': 'application/json'
      }))
    };
    return this.http.patch(url, updates, httpOptions);
  }
  // normal user
  // ==================================================
  accountTopup(app, body){
    const url = `${app.apiUrl}/user/account/top-up`;
    const headers = app.requestHeaders();
    const httpOptions = {
      headers: new HttpHeaders(
        _.extend({}, headers, {
        'Accept': 'application/json', 'Content-Type': 'application/json'
      }))
    };
    return this.http.post(url, body, httpOptions);
  }
    getProducts(app) {
    const url = `${app.apiUrl}/user/products`;
    const headers = app.requestHeaders();
    const httpOptions = {
      headers: new HttpHeaders(
        _.extend({}, headers, {
        'Accept': 'application/json', 'Content-Type': 'application/json'
      }))
    };
    return this.http.get(url, httpOptions);
  }
  getAccount(app) {
      const headers = app.requestHeaders();
      const url = `${app.apiUrl}/user/${app.localUser.id}/account`;
      const httpOptions = {
        headers: new HttpHeaders(
          _.extend({}, headers, {
            'Accept': 'application/json', 'Content-Type': 'application/json'
          }))
      };
      return this.http.get(url, httpOptions);
  }

   purchaseProduct(app, data) {
    const url = `${app.apiUrl}/user/product/purchase`;
    const headers = app.requestHeaders();
    const httpOptions = {
      headers: new HttpHeaders(
        _.extend({}, headers, {
        'Accept': 'application/json', 'Content-Type': 'application/json'
      }))
    };
    return this.http.post(url, data, httpOptions);
  }

  // auth
  // ==================================================
  login(app, credentials) {
    const url = `${app.apiUrl}/auth/login`;
    const headers = app.requestHeaders();
    const httpOptions = {
      headers: new HttpHeaders(
        _.extend({}, headers, {
        'Accept': 'application/json', 'Content-Type': 'application/json'
      }))
    };
    return this.http.post(url, credentials, httpOptions);
  }

  signup(app, data) {
    const url = `${app.apiUrl}/auth/signup`;
    const headers = app.requestHeaders();
    const httpOptions = {
      headers: new HttpHeaders(
        _.extend({}, headers, {
       'Accept': 'application/json', 'Content-Type': 'application/json'
      }))
    };
    return this.http.post(url, data, httpOptions);
  }
  validateToken(app) {
    const url = `${app.apiUrl}/auth/validate-token`;
    const headers = app.requestHeaders();
    const httpOptions = {
      headers: new HttpHeaders(
        _.extend({}, headers, {
        'Accept': 'application/json', 'Content-Type': 'application/json'
      }))
    };
    return this.http.post(url, {}, httpOptions);
  }
}
