import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {NavigationEnd, Router} from '@angular/router';
import _ from 'lodash';

import {ApiService} from './api.service';

/*
 const apiUrl =  'http://localhost:9000';
 const appId =  process.env.APP_ID || 'spaza_web_client';
 const systemId = process.env.SYSTEM_ID || 'spazaserve';
 */
const apiUrl = 'http://localhost:9000';
const appId = 'spaza_web_client';
const systemId = 'spazaserve';
const tokenKey = 'x_nekot_x';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  apiUrl: string;
  app;
  appId: string;
  systemId: string;
  token: string;
  tokenKey: string;
  localUser;
  user = new BehaviorSubject<any>({});
  castUser = this.user.asObservable();
  currentRoutePath = new BehaviorSubject<string>('');
  castCurrentRoutePath = this.currentRoutePath.asObservable();


  constructor(private _apiService: ApiService, private router: Router) {

  }

  init() {
    this.apiUrl = apiUrl;
    this.app = this;
    this.appId = appId;
    this.systemId = systemId;
    this.tokenKey = tokenKey;
    this.getToken();
    this.onUserChanged = this.onUserChanged.bind(this);
    this.requestHeaders = this.requestHeaders.bind(this);
    this.tokenSave = this.tokenSave.bind(this);
    this._validateToken();

    this.router.events.subscribe((event) => {
       if (event instanceof NavigationEnd) {
         this.currentRoutePath.next(event.url);
       }
     });
  }

  getApp() {
    return this.app;
  }

  getToken() {
    const token = localStorage.getItem(this.tokenKey);
    if (token) {
      this.token = token ;
    } else {
      this.token = '';
    }
  }

  onUserChanged(newUser) {
    this.localUser = newUser;
    this.user.next(newUser);
  }

  requestHeaders() {
    const user = this.localUser;
    return _.extend({}, {
        Authorization: this.token,
        systemId: this.systemId,
        appId: this.appId
      },
      user ? {userId: user.id, username: user.username} : {},
    );
  }

  resetApp() {
    localStorage.clear();
    this.router.navigate(['/login'], {replaceUrl: true});
    window.location.reload();
  }

  tokenSave(token) {
    try {
      localStorage.setItem(this.tokenKey, token);
      this.token = token;
    } catch (e) {
    }
  }

  _validateToken() {
    this._apiService.validateToken(this.app).subscribe(responseData => {
      if (responseData['success']) {
        this.loginSuccessful(responseData);
      } else {
        this.router.navigate(['/login'], {replaceUrl: true});
      }
    });
  }

  loginSuccessful(data) {
    this.onUserChanged(data.item);
    this.tokenSave(data.token);
    this.castCurrentRoutePath.subscribe((path) => {
      if (path.includes('/login') || path.includes('/signup')) {
        this.router.navigate(['/products'], {replaceUrl: true});
      }
    });
  }
}
