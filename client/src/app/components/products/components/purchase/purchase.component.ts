import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute , Router} from '@angular/router';
import _ from 'lodash';

import {AccountService} from '../../../../services/account.service';
import {ProductService} from '../../../../services/product.service';

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.css']
})
export class PurchaseComponent implements OnInit {
   product;
   account;
   quantity = 1;
  constructor(private accountService: AccountService, private productService: ProductService, private route: ActivatedRoute, private router: Router ) { }

  ngOnInit() {
    const that = this;
    this.route.params.subscribe((params) => {
      that.productService.getProduct(params.id).then((product) => {
        that.product = _.extend({}, that.product , product);
      }).catch(e => {
        console.log('UpdateProductComponent.err ===> ', e);
      });
    });
    that.accountService.getAccount().then((account)=>{
       that.account = account;
    }).catch((err) => {
      console.log('PurchaseComponent.ngOnInit.err ===>', err);
    });
  }

  onPurchase(){
    this.productService.purchaseProduct(this.product, this.quantity)
      .subscribe((responseData) => {
        if (responseData['success']) {
            this.account = responseData['item'];
            this.accountService.setAccount(this.account);
            this.router.navigate(['/products']);
        } else {
          alert(responseData['message']);
        }
    });
  }
}
