import {Component, OnInit} from '@angular/core';
import _ from 'lodash';

import {ProductService} from '../../services/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products;

  constructor(private productService: ProductService) {
  }

  ngOnInit() {
      this.products = this.productService.getProducts().subscribe((responseData) => {
      if (responseData['success']) {
        this.products = responseData['items'];
        this.productService.setProducts(this.products);
      } else {
        alert(JSON.stringify(responseData['message']));
      }
    });
  }
}
interface Product {
  id: string,
  name: string,
  lastname: string,
  description: string,
  price: number,
  discount: number,
}
