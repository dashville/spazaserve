import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  form = {
    name: '',
    lastname: '',
    phone: '',
    email: '',
    password: ''
  };
  errorMessage: '';
  constructor(private authService: AuthService) {
  }

  onSignUp() {
    const form = (this.form);
    this.authService.singup(form).then(() => {}).catch((e) => {
      this.errorMessage = e;
    });
  }

  ngOnInit() {
  }
}
