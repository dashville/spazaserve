import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import {AccountService} from '../../services/account.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  account;
  amount;
  constructor(private accountService: AccountService) { }

  ngOnInit() {
    this.amount = 0;
    this.accountService.getAccount().then((account) => {
      this.account = account;
    }).catch((e) => {

    });
  }

  accountTopup() {
   if (this.amount > 0) {
     this.accountService.accountTopup(this.account.id, this.amount).then((account) => {
       this.account = account;
     }).catch((err) => {
       alert(err);
     });
   }
  }

  formatDate(tstamp){
    return moment(Number(tstamp)).format('DD MMM YYYY, HH:mm');
  }

  Round(amount) {
    return (Math.round(amount * 100) / 100);
  }

}
