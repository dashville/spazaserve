import {Component, OnInit, Input} from '@angular/core';

import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  credentials = {
    username: '',
    password: ''
  };
  errorMessage;

  constructor(private authService: AuthService) {
  }

  onLogin() {
    const credentials = this.credentials;
    this.authService.login(credentials).then(() => {}).catch((e) => {
      this.errorMessage = e;
    });
  }

  ngOnInit() {
  }

}
