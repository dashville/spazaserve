import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import _ from 'lodash'

import {AdminService} from '../../../services/admin.service'
@Component({
  selector: 'app-purchases',
  templateUrl: './purchases.component.html',
  styleUrls: ['./purchases.component.css']
})
export class PurchasesComponent implements OnInit {
  purchases;
  constructor(private adminService:  AdminService) { }

  ngOnInit() {
    this.adminService.getPurchases().subscribe((responseData) => {
      if (responseData['success']) {
        this.purchases = _.reverse(responseData['items']);
      } else {
        alert(responseData['message']);
      }
    });
  }

  formatDate(tstamp){
    return moment(Number(tstamp)).format('DD MMM YYYY, HH:mm');
  }

  Round(amount) {
    return (Math.round(amount * 100) / 100);
  }

}
