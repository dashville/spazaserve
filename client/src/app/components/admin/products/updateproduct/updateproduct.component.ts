import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import _ from 'lodash';

import {AdminService} from '../../../../services/admin.service';
import {ProductService} from '../../../../services/product.service';

const defaultForm = {
    id: '',
    brand: '',
    name: '',
    price: '',
    description: ''
  };


@Component({
  selector: 'app-updateproduct',
  templateUrl: './updateproduct.component.html',
  styleUrls: ['./updateproduct.component.css']
})
export class UpdateproductComponent implements OnInit {
  form = defaultForm;
  successMessage;
  errorMessage;
  id;

  constructor(private adminService: AdminService, private productService: ProductService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    const that = this
    this.route.params.subscribe((params) => {
      this.id = params.id;
      that.productService.getProduct(this.id).then((product) => {
        that.form = _.extend({}, that.form , product);
      }).catch(e => {
        console.log('UpdateProductComponent.err ===> ', e);
      });
    });
  }

  updateProduct() {
    const {form} = this
    this.adminService.updateProduct(this.id, _.omit(form, ['id'])).subscribe((responseData) => {
      if (responseData['success']) {
        this.successMessage = 'Product successfully updated';
        this.errorMessage = '';
        this.form = _.extend({}, responseData['item']);
      } else {
        this.errorMessage = responseData['message'];
        this.successMessage = '';
      }
    });
  }
  textAreaOnChange(e) {
    this.form = _.extend({}, this.form, {description: e.target.value});
  }
}
