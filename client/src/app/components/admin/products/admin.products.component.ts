import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import _ from 'lodash';

import {ProductService} from '../../../services/product.service';
import {AdminService} from '../../../services/admin.service'
@Component({
  selector: 'app-admin-products',
  templateUrl: './admin.products.component.html',
  styleUrls: ['./admin.products.component.css']
})
export class AdminProductsComponent implements OnInit {
  products;
  constructor(private productService: ProductService, private adminService: AdminService) { }

  ngOnInit() {
    this.products = this.productService.getProducts().subscribe((responseData) => {
      if (responseData['success']) {
        this.products = _.reverse(responseData['items']);
        this.productService.setProducts(this.products);
      }
    });
  }

  deleteProduct(id) {
    this.adminService.removeProduct(id).subscribe((responseData) => {
      if (responseData['success']) {
        this.products = _.filter(this.products, product => product.id !== id);
        this.productService.setProducts(this.products);
      }
    });
  }
  formatDate(tstamp) {
    return moment(Number(tstamp)).format('Do MMM YY, HH:mm');
  }
}
