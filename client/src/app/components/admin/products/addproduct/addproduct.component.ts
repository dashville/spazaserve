import { Component, OnInit } from '@angular/core';
import {AdminService} from '../../../../services/admin.service';

import _ from 'lodash';
const defaultForm = {
    brand: '',
    name: '',
    price: '',
    description: ''
  };

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css']
})

export class AddproductComponent implements OnInit {
  form = defaultForm;
  successMessage;
  errorMessage;
  constructor(private adminService:  AdminService) { }

  ngOnInit() {
  }
  addProduct() {
    this.adminService.addProduct(this.form).subscribe((responseData) => {
      if (responseData['success']) {
         this.form = defaultForm;
         this.successMessage = 'Product added successfully';
          this.errorMessage = '';
      } else {
        this.errorMessage = responseData['message'];
        this.successMessage = '';
      }
    });
  }
  textAreaOnChange(e) {
    this.form = _.extend({}, this.form, {description: e.target.value});
  }
}
