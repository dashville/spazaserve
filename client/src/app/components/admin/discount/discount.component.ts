import {Component, OnInit} from '@angular/core';
import _ from 'lodash';
import {AdminService} from '../../../services/admin.service';
@Component({
  selector: 'app-discount',
  templateUrl: './discount.component.html',
  styleUrls: ['./discount.component.css']
})
export class DiscountComponent implements OnInit {
  discounts;

  constructor(private adminService: AdminService) {
  }

  ngOnInit() {
    this.adminService.getDiscounts().subscribe((responseData) => {
      if (responseData['success']) {
        this.discounts = responseData['items'];
        if (this.discounts.length === 0) {
          alert('No discounts, load initial discounts using a script');
        }
      } else {
        alert(responseData['message']);
      }
    });
  }

  updateDiscount(id, rate) {
    this.adminService.updateDiscount(id,rate).subscribe((responseData) => {
      if (responseData['success']) {
        this.discounts = _.map(this.discounts, (d) => {
          if (d.id === id) {
            return _.extend({}, d, responseData['item']);
          }
          return d;
        });
        alert('Update successful');
      } else {
        alert(responseData['message']);
      }
    });
  }

}
