import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import _ from 'lodash';

import {AppService} from '../../services/app.service';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  app;
  user;
  collapsed = true;
  userDropdown = true;
  currentRoutePath;
  isAdminPath = false;
  isAuthPath = false;

  constructor(private appService: AppService, private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.app = this.appService.getApp();
    this.appService.castUser.subscribe((user) => {
      this.user = user;
    });
    this.appService.castCurrentRoutePath.subscribe((path) => {
      this.currentRoutePath = path;
      this.isAdminPath = path.includes('/admin');
      this.isAuthPath = path.includes('/login') || path.includes('/resetpassowrd');
    });
  }


  hasUser(){
   return !_.isEmpty(this.user);
  }
  logout() {
    this.authService.logout();
  }

  toggleCollapsed(): void {
    this.collapsed = !this.collapsed;
  }
  toggleUserDropdown(): void {
    this.userDropdown = !this.userDropdown;
  }
}
