import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ProductsComponent} from './components/products/products.component';
import {LoginComponent} from './components/login/login.component';
import {SignupComponent} from './components/signup/signup.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {AccountComponent} from './components/account/account.component';
import {PurchaseComponent} from './components/products/components/purchase/purchase.component';

import {AccountService} from './services/account.service';
import {AdminService} from './services/admin.service';
import {AppService} from './services/app.service';
import {ApiService} from './services/api.service';
import {AuthService} from './services/auth.service';
import {ProductService} from './services/product.service';

import { PurchasesComponent } from './components/admin/purchases/purchases.component';
import { DiscountComponent } from './components/admin/discount/discount.component';
import { AddproductComponent } from './components/admin/products/addproduct/addproduct.component';
import { UpdateproductComponent } from './components/admin/products/updateproduct/updateproduct.component';
import { AdminProductsComponent} from './components/admin/products/admin.products.component';

@NgModule({
  declarations: [
    AppComponent,
    AccountComponent,
    LoginComponent,
    NavbarComponent,
    ProductsComponent,
    PurchaseComponent,
    SignupComponent,
    PurchasesComponent,
    DiscountComponent,
    AddproductComponent,
    UpdateproductComponent,
    AdminProductsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    AccountService,
    AdminService,
    AppService,
    ApiService,
    AuthService,
    ProductService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
