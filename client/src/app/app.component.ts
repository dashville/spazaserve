import {Component, OnInit} from '@angular/core';
import {AppService} from './services/app.service';
import 'jquery';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  constructor(private _appService: AppService){
     _appService.init();
  }
}
