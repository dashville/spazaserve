import express from 'express';
import AuthUtils from '../utils/AuthUtils';
import AdminController from '../controllers/AdminController'

const router = new express.Router();

router.route('*').all(AuthUtils.authenticate);
router.route('/product/add').post(AdminController.addProduct);
router.route('/product/:id/remove').delete(AdminController.removeProduct);
router.route('/product/:id/update').patch(AdminController.updateProduct);
router.route('/discount/update').patch(AdminController.updateDiscount);
router.route('/discounts').get(AdminController.retrieveDiscounts);
router.route('/purchases').get(AdminController.retrievePurchases);

export default router