import express from 'express';
import {AuthController} from '../controllers/index';

const router = new express.Router();

router.route('/login').post(AuthController.login);
router.route('/signup').post(AuthController.signup);
router.route('/validate-token').post(AuthController.validateToken);
router.route('/password-reset').post(AuthController.resetPassword);
router.route('/password-reset-request').post(AuthController.requestToResetPassword);

export default router