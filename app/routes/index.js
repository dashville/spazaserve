import adminRouter from './admin.route';
import authRouter from './auth.route';
import userRouter from './user.route';

export {
    adminRouter,
    authRouter,
    userRouter
}