import express from 'express';
import AuthUtils from '../utils/AuthUtils';
import {UserController} from '../controllers/index';

const router = new express.Router();
router.route('*').all(AuthUtils.authenticate);
router.route('/:id/account').get(UserController.retrieveAccount);
router.route('/account/top-up').post(UserController.topupAccount);
router.route('/products').get(UserController.retrieveProducts);
router.route('/product/purchase').post(UserController.purchase);


export default router