import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import logger from 'morgan'
import passport from 'passport'
import cors from  'cors'

import {connect} from './config/init'
import {adminRouter, authRouter, userRouter} from './routes/index';
import AuthUtils from './utils/AuthUtils'

const app = new express();
const PORT = process.env.PORT || 9000;

connect();
passport.use(AuthUtils.strategy());

app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname,'public')));
app.use(passport.initialize());

app.use('/auth', authRouter);
app.use('/admin', adminRouter);
app.use('/user', userRouter);

app.get('/', (req,res)=>{
    res.send("api running")
});

app.listen(PORT,()=>{
    console.log(`*** Server running on port ${PORT} ***`,);
});