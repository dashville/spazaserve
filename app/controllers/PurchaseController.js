import _ from 'lodash';
import {Purchase, Account} from '../models/index';

class PurchaseController {
    constructor() {
        this.processPurchase = this._processPurchase.bind(this);
        this.retrievePurchases  = this._retrievePurchases.bind(this)
    }

    _createPurchase(data) {
        return new Promise((resolve, reject) => {
            Purchase.create(data, (err, purchase) => {
                if (err) reject({code: 500, message: "Could't perform purchase", err});
                resolve(purchase)
            })
        })
    }

    _getAccount(userId, total) {
        return new Promise((resolve, reject) => {
             console.log('PurchaseController.purchase.body 1 ===>')
            Account.findOne({'holder.id': userId}, (err, account) => {
                if (err) {
                    reject({code: 500, message: "Could't retrieve account", err})
                }
                console.log('PurchaseController.purchase.body 2 ===>', account)

                if (account) {
                    if (account.balance > total) {
                        resolve(account)
                    } else {
                        reject({code: 406, message: "Insufficient funds"})
                    }
                } else {
                    reject({code: 404, message: "No account found"});
                }
            })
        })
    }

    _processPurchase(req, res, next) {
        const handleError = (e) => {
            res.send({success: false, message: e.message})
        };
        const purchaseRollBack = (id)=>{
            try {
                Purchase.remove({_id: id}, (err, result) => {
                    if (err) console.log('PurchaseController.rollback ===>', err);
                });
            } catch (e){
                console.log('PurchaseController.rollback.failed ***')
            }
        }
        const transformPurchaseData = (user, product, quantity, count) =>{
            return{
                quantity, 'purchase-no': count,
                product: {
                    id: product.id,
                    names: product.brand + ' ' + product.name,
                    price: (product.price),
                },
                createdBy: {
                    id: user.id,
                    names: user.name + ' ' + user.lastname
                }
            }
        }

        const transectAccount = (account, data, amount)=>{
                account.balance = account.balance - amount;
                account.transactions = _.union(account.transactions, [{
                    createdOn: new Date().getTime(),
                    description: `Purchase:  ${data.product.names}`,
                    amount: -Math.abs(amount)
                }]);

                let saved = true
                account.save((err) => {
                    if (err) {
                        res.status(500).send({success: true, message: "Couldn't deduct from account"});
                        purchaseRollBack(purchase._id || purchase.id);
                    }
                });
                if(saved){
                    return account
                }

        }
        const {user, product, quantity} = req.body;
        const total =  (product.price - (product.discount || 0)) * quantity;

        this._getAccount(user.id, total).then(function(account) {
           Purchase.count({}, function( err, count){
                 if(err) res.status(500).send({success:false, message: 'Failed process purchase'})

                const data = transformPurchaseData(user, product, quantity, count)
                Purchase.create(data, function(err, purchase) {
                    if (err)handleError({code: 500, message: "Could't perform purchase"});
                    const updatedAccount = transectAccount(account, data, total, purchase)

                    res.send({success: true, item: updatedAccount})
                    });
                });
        }).catch(handleError);
    }

    _retrievePurchases(req,res){
        Purchase.find({}, (err,purchases)=>{
            if(err) res.send({success: false, message: "Couldn't get purchases."});
            res.send({success: true, items: purchases});
        });
    }
}

export default new PurchaseController();