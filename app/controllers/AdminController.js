import {Product, Discount, Purchase} from '../models/index'
import {PurchaseController} from './index'
class AdminController {
    constructor() {
         this.addProduct = this._addProduct.bind(this);
         this.removeProduct = this._removeProduct.bind(this);
         this.retrieveDiscounts = this._retrieveDiscounts.bind(this);
         this.retrievePurchases = this._retrievePurchases.bind(this)
         this.updateProduct = this._updateProduct.bind(this);
         this.updateDiscount = this._updateDiscount.bind(this);
    }

    _addProduct(req, res) {
        const data = this._transformProdcutData(req.body);

        Product.create(data, (err, product)=>{
            if(err) res.send({success: false, message: "Could't add product"});
            res.send({success: true, item: product})
        });
    }

    _updateProduct(req, res) {
        const {id} = req.params;
        const updates =  req.body;
        Product.findOneAndUpdate({_id: id}, updates,{new:true},(err, product)=>{
            if(err) res.send({success: false, message: "Could't update product"});
            res.send({success: true, item: product})
        })
    }
    _removeProduct(req,res){
        const {id} = req.params;
        Product.deleteOne({_id: id}, (err, product)=>{
           if(err) res.send({success: false, message: "Could't delete product"});
            res.send({success: true, item: id})
        });
    }
     _retrieveDiscounts(req,res) {
            Discount.find({}, (err, discounts) => {
                if(err) res.send({success: false, message: "Could't retreive discounts"});
                res.send({success: true, items: discounts})
     });
    }
    _retrievePurchases(req,res) {
       PurchaseController.retrievePurchases(req,res)
    }

    _transformProdcutData(data) {
        const description = data && data.description ? data.description.trim() : undefined
        return {
            brand: data.brand.trim(),
            name: data.name.trim(),
            description,
            price: Number(data.price),
        }
    }
    _updateDiscount(req,res){
        const {id, rate} = req.body;
        const updates = {
            updatedOn: new Date().getTime(),
            rate
        }
        Discount.update({_id: id},updates,{new:true},(err,discount)=>{
            if(err) res.send({success: false, message: "Could't update discount"})
            res.send({success: true, item: discount})
        })
    }

}
export default new AdminController();