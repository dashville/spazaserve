import AdminController from './AdminController';
import AuthController from './AuthController';
import DiscountController from './DiscountController'
import PurchaseController from './PurchaseController'
import UserController from './UserController';

export {
    AdminController,
    AuthController,
    DiscountController,
    PurchaseController,
    UserController
}