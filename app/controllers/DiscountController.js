import {Discount} from '../models/index';
import _ from 'lodash';
import {discountRules} from '../config/rules'


class DiscountController {
    constructor(){
        this.determineDiscount = this._determineDiscount.bind(this);
        this.retrieveDiscounts = this._retrieveDiscounts.bind(this);

    }
    _determineDiscount(price, discounts) {
        let discountAmount = 0;
        let discount;

        if (discounts.length > 0) {
            if (price >= 50 && price <= 100) {
                discount = _.find(discounts, d => {return d.rule === discountRules[0]});
                discountAmount = price - (price * ((discount && discount.rate) / 100 || 0))

            } else if (price >= 112 && price <= 115){
                discount = _.find(discounts, d => {return d.rule === discountRules[1]});
                discountAmount = price - (price * ((discount && discount.rate ) / 100|| 0))

            } else if (price > 120) {
               discount = _.find(discounts, d => {return d.rule === discountRules[2]});
               discountAmount = price - (price * ((discount && discount.rate) / 100 || 0))
            }
        }

        return discountAmount;
    }
    _retrieveDiscounts() {
        return new Promise((resolve, reject) => {
            Discount.find({}, (err, discounts) => {
                if(err) reject(err);
                resolve(discounts || []);
            });
        });
    }

}

export default new DiscountController();