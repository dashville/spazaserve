import {Account, User} from '../models/index';
import AuthUtils from '../utils/AuthUtils'
import UUID from 'uuid/v1';

class AuthController {
    constructor() {
        this.login = this._login.bind(this);
        this.logout = this._logout.bind(this);
        this.requestToResetPassword = this._requestToResetPassword.bind(this)
        this.resetPassword = this._resetPassword.bind(this);
        this.signup = this._signup.bind(this);
        this.validateToken = this._validateToken.bind(this);
    }

    _createAccount(user,res){
        const holder = {
            id:  user.id || user._id,
            names: user.name + ' ' + user.lastname,
        }
        Account.create({holder}, (err, account)=>{
             if (err) res.send({success: false, message: err});
             this._onLoginSuccessful(user, res);
        })
    }

    _createUser(data, res) {
        const pwd = data.password.trim()

        AuthUtils.encryptpwd(pwd).then((hashedPwd) => {
            return this._transformUserData(data, hashedPwd)
        }).then((userData) => {
            User.create(userData, (err, user) => {
                if (err) res.send({success: false, message: "Couldn't sign up"});
                this._createAccount(user,res)
            })
        }).catch((err) => {
            console.log('AuthController._createUser.error ===> ', err)
        })
    }

    _login(req, res) {
        const credentials = req.body;
        const username = credentials.username.toLowerCase().trim();
        const pwd = credentials.password;

        User.findOne({username}, (err, user) => {
            if (err) res.send({success: false, message: err});
            if (user) {
                AuthUtils.comparepwd(pwd, user.password).then((isMatch) => {
                    if (isMatch) {
                        if(user && user.hasAccount){
                            this._onLoginSuccessful(user, res);
                        } else {
                            this._createAccount(user, res);
                        }
                    } else {
                        res.send({success: false, message: "Invalid username or password."});
                    }
                });
            } else {
                res.send({success: false, message: "Couldn't find your user account."});
            }
        });
    }

    _logout(req, res) {
        //destroy token clientside
    }

    _onLoginSuccessful(user, res) {

        /*
         iss:  spazaserve (issuer)
         sub: userid (subject)
         aud: spazawebapp (audience)
         exp: time (expirationtime)
         iat: (issued at)
         jti (jwt id)
         */

        const payload = {
            iss: 'spazaserve',
            sub: user.id || user._id,
            pwd: user.password,
            admin: user.admin,
            iat: new Date().getTime(),
            jti: UUID()
        }

        AuthUtils.generateToken(payload).then((token) => {
            return res.json({success: true, item: user, token})
        }).catch((err) => {
                    res.send({success: false, message: err})
            console.log('Authenticate._onLoginSuccessful. err ->', err)
        })
    }

    _signup(req, res) {
        const data = req.body;
        const email = data.email.trim().toLowerCase();

        User.findOne({username: email}, (err, user) => {
            if (err) res.send({success: false, message: err});
            if (user) {
                res.json({success: false, message: 'Email already taken.'})
            } else {
                this._createUser(data, res);
            }
        })
    }

    _requestToResetPassword(req, res) {
        const {username} = req.body
        User.findOne({username}, (err, user) => {
            if (err) res.send({success: false, message: err});
            if (user) {
                res.send({success: true, item: user._id || user.id});
            } else {
                res.send({success: false, message: "Username not registered."});
            }
        })
    }

    _resetPassword(req, res) {
        const {id, password} = req.body;

        User.findOne({_id: id}, (err, user) => {
            if (err) res.send({success: false, message: err});
            if (user) {

                AuthUtils.encryptpwd(password.trim()).then((hashedPwd) => {
                    user.password = hashedPwd;
                    user.updatedOn = new Date().getTime();
                    user.save((err)=>{
                        if (err) res.send({success: false, message: err});
                        res.send({success: true, item: user.id});
                    })
                }).catch((err)=>{
                   res.send({success: false, message: err});
                });
            }
        });
    }

    _transformUserData(data, hashedPwd) {
        const email = data.email.toLowerCase().trim();
        return {
            email,
            username: email,
            password: hashedPwd,
            name: data.name.trim(),
            lastname: data.lastname.trim(),
            phone: data.phone.trim(),
        }
    }

    _validateToken(req, res) {
        const {authorization} = req.headers;
        if (authorization) {
            AuthUtils.verifyToken(authorization).then((decodedToken) => {
                if (decodedToken) {
                    User.findOne({_id: decodedToken.sub}, (err, user) => {
                        if (err) res.send({success: false, message: err});
                        console.log('AuthController.user ===>', user)

                        if (user && decodedToken.pwd === user.password) {
                            return res.send({success: true, item: user, token: authorization});
                        } else {
                            res.send({success: false, message: "Invalid username or password."});
                        }
                    })
                } else {
                    res.send({success: false, message: "Token authentication failed."})
                }
            }).catch((err) => {
                res.send({success: false, message: 'Token needed'})
                console.log('AuthController._validateToken.err ==> ', err)
            })
        } else {

             res.send({success: false, message: "Token authentication failed."})
        }
    }
}
export default new AuthController();