import {Account, Product, Purchase} from '../models/index';
import {PurchaseController} from './index'
import {DiscountController} from '../controllers/index';
import _ from 'lodash';

class UserController {
    constructor() {
        this.purchase = this._purchase.bind(this)
        this.retrieveAccount = this._retrieveAccount.bind(this);
        this.retrieveProducts = this._retrieveProducts.bind(this)
        this.topupAccount = this._topupAccount.bind(this);
    }

    _purchase(req, res, next) {
        PurchaseController.processPurchase(req,res, next);
    }

    _retrieveAccount(req, res) {
        const {id} = req.params
        Account.findOne({'holder.id': id}, (err, account)=> {
            if(err) res.send({success: false, message: err});
        res.send({success: true, item: account})
    });
    }

    _retrieveProducts(req, res) {
        const transform = (p) => {
            const product = p.toObject()
            product.id = product._id;
                delete product._id;
                delete product.__v;
            return product
        }
        const getProducts = (discounts) => {
            const filters = {_id: 1, brand: 1, name: 1, description: 1, price: 1, createdOn: 1, updatedOn: 1}
            Product.find({}, filters, (err, products) => {
                if(err) res.send({success: false, message: err});
                const items = _.map(products,p=>{
                         const discount = DiscountController.determineDiscount(p.price,discounts);
                        return _.extend({},transform(p), {discount});
                    })
                res.send({success: true, items})
            });
        }
        DiscountController.retrieveDiscounts().then(discounts=>{
            getProducts(discounts);
        }).catch((e)=>{
            console.log('UserController._retrieveProducts.err ===>', err)
            getProducts([])
        })
    }

    _topupAccount(req, res) {
        const {id, amount} = req.body;
        const transaction = {
            createdOn: new Date().getTime(),
            description: 'Topup: ', amount
        };

        const update = {
            $inc: {balance: Number(amount)},
            $push: {transactions: transaction}
        };

        Account.findOneAndUpdate({_id: id}, update, {new: true}, (err, account)=> {
            if(err) res.send({success: false, message: "Couldn't top up account"});
            res.send({success: true, item: account})
        });
    }
}

export default new UserController();