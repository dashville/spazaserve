const serializer = {
    transformToJSON(schema){
        schema.set('toJSON', {
            transform: function (doc, ret, options) {
                ret.id = ret._id;
                delete ret._id;
                delete ret.__v;
            }
        })
    }
}
export default serializer;