import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import {Strategy, ExtractJwt} from 'passport-jwt';
import passport from 'passport';
import _ from 'lodash'

import {User} from '../models/index'

const SALT_ROUNDS = 10;
let config = {secret: "xTestingx"};

class AuthUtils {
    constructor() {
        this.authenticate = this._authenticate.bind(this);
        this.comparepwd = this._comparepwd.bind(this);
        this.encryptpwd = this._encryptpwd.bind(this);
        this.generateToken = this._generateToken.bind(this);
        this.strategy = this._setStrategy.bind(this);
        this.verifyToken = this._verifyToken.bind(this);
    }

    _authenticate(req, res, next) {
        //const excluded = ['/', '/login'];
       passport.authenticate('jwt', {session: false}, (err, user, info) => {
            if (err) {
                res.send({success: false, message: err})
            }
            if (user) {
                if (!user.admin && req.baseUrl.includes('/admin')) {
                    return res.status(401).json({
                        success: false, message: 'User Unauthorized',
                        status: 'error', code: 'Unauthorized'
                    })
                }
                req.user = user;
                return next()
            } else {
               return res.status(401).json({
                    success: false, message: 'User Unauthorized',
                    status: 'error', code: 'Unauthorized'
                })
            }
        })(req, res, next);
    }

    _comparepwd(pwd, hash) {
        return new Promise((resolve, reject) => {
            bcrypt.compare(pwd, hash, (err, isMatch) => {
                if (err) reject(err)
                resolve(isMatch);
            })
        })
    }

    _encryptpwd(pwd) {
        return new Promise((resolve, reject) => {
            bcrypt.hash(pwd, SALT_ROUNDS, (err, hash) => {
                if (err) reject(err);
                resolve(hash)
            })
        })
    }

    _generateToken(payload) {
        return new Promise((resolve, reject) => {
            jwt.sign(payload, config.secret, (err, token) => {
                if (err) reject(err);
                resolve(token)
            })
        })
    }

    _setStrategy() {
        let jwtOptions = {}
        jwtOptions.jwtFromRequest = ExtractJwt.fromHeader('authorization');
        jwtOptions.secretOrKey = config.secret
        jwtOptions.issuer = 'spazaserve';

       return new Strategy(jwtOptions, (jwt_payload, next) => {
            User.findOne({_id: jwt_payload.sub}, (err, user) => {
                if (err) {
                     return next(err, false)
                }
                if (user) {
                    return next(null, user);
                }
                return next(null, false);
            });
        });
    }

    _verifyToken(token) {
        return new Promise((resolve, reject) => {
            jwt.verify(token, config.secret, (err, decoded) => {
                if (err) reject(err);
                resolve(decoded)
            })
        })
    }
}
export default new AuthUtils();