import mongoose from 'mongoose';

export const connect = () => {
    const DB_URL = process.env.DB_URL || 'mongodb://localhost:27017/spaza_db';

    mongoose.Promise = global.Promise;
    mongoose.connect(DB_URL).then(() => {
        console.log(' *** db connection successful ***');
    }).catch((e) => {
        console.log(' *** db connection unsuccessful ***',e);
    })
};

export const roles = {
    user: {role: 'user', description:'User'},
    admin: {role: 'admin', description:'Administrator'}
};