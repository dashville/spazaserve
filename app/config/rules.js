import {Discount} from '../models/index'
const rules = [
    'between 50 and 100, both inclusive',
    'between 112 and 115, both inclusive',
    'greater than 120'
];
export const discountRules = process.env.DISCOUNT_RULES || rules

/*
    db.discounts.insert({rule:'between 50 and 100, both inclusive', rate: 0})
    db.discounts.insert({rule: 'between 112 and 115, both inclusive', rate: 25})
    db.discounts.insert({rule:'greater than 120', rate: 50})
*/
