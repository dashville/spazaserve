import mongoose from 'mongoose';
import serializer from '../utils/serializer';

const purchaseSchema = mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    'purchase-no': String,
    quantity: {type: Number, required: [true, "can't be blank"]},
    product: {
        id: String,
        names: String,
        price: Number,
    },
    createdOn: {type: Number, default: new Date().getTime()},
    createdBy: {
        id: String,
        names: String
    }
});
serializer.transformToJSON(purchaseSchema);

export default mongoose.model('Purchase', purchaseSchema);