import Account from './account.model';
import Inventory from './inventory.model';
import Product from './product.model';
import Purchase from './purchase.model';
import User from './user.model';
import Discount from './discount.models'

export {
    Account,
    Discount,
    Inventory,
    Product,
    Purchase,
    User
}