import mongoose from 'mongoose';
import serializer from '../utils/serializer'

const inventorySchema = mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    productId: {type: String, required: [true, "can't be blank"]},
    quantity: {type: Number, required: [true, "can't be blank"]},
    createdOn: {type: Number, default: new Date().getTime()},
    updatedOn: Number,
});
serializer.transformToJSON(inventorySchema);

export default mongoose.model('Inventory', inventorySchema);