import mongoose from 'mongoose';
import {roles} from '../config/init'
import serializer from '../utils/serializer'

const userSchema = mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    username: {
        type: String, lowercase: true,
        required: [true, "can't be blank"],
        unique: true,
        match: [/\S+@\S+\.\S+/, 'is invalid'],
        index: true
    },
    email: {
        type: String, lowercase: true,
        required: [true, "can't be blank"],
        unique: true,
        match: [/\S+@\S+\.\S+/, 'is invalid'],
        index: true
    },
    name: {type: String, required: [true, "can't be blank"]},
    lastname: {type: String, required: [true, "can't be blank"]},
    password: {type: String, required: [true, "can't be blank"]},
    phone: {type: String, required: [true, "can't be blank"]},
    createdOn: {type: Number, default: new Date().getTime()},
    updatedOn: Number,
    admin: {type: Boolean, default: false},
    roles: {type: Array, default: [roles.user]},
    hasAccount: {type: Boolean, default: false}
});

serializer.transformToJSON(userSchema);

export default mongoose.model('User', userSchema);
