import mongoose from 'mongoose';
import serializer from '../utils/serializer'

const discountSchema = mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    rule:  {type: String, required: [true, "can't be blank"]},
    rate : {type: Number, required: [true, "can't be blank"]},
    createdOn: {type: Number, default: new Date().getTime()},
    updateOn: Number,
    discount: Number
});

serializer.transformToJSON(discountSchema);

export default mongoose.model('Discount', discountSchema);