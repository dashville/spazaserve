import mongoose from 'mongoose';
import serializer from '../utils/serializer';

const productSchema = mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    brand: {type: String, required: [true, "can't be blank"]},
    name: {type: String, required: [true, "can't be blank"]},
    description: String,
    price: {type: Number, required: [true, "can't be blank"]},
    createdOn: {type: Number, default: new Date().getTime()},
    updatedOn: Number,
});

serializer.transformToJSON(productSchema);

export default mongoose.model('Product', productSchema);