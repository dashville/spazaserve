import mongoose from 'mongoose';
import serializer from '../utils/serializer'

const accountSchema = mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    balance: {type: Number, default: 0},
    createdOn: {type: Number, default: new Date().getTime()},
    updatedOn: Number,
    holder: {
        id: String,
        names: String
    },
    transactions: [{
         createdOn: String,
         description: String,
         amount: Number
    }]
});

serializer.transformToJSON(accountSchema);

export default mongoose.model('Account', accountSchema);